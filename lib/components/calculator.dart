import 'package:flutter/material.dart';

class Calculator extends StatefulWidget {
  Calculator({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _CalculatorState createState() => _CalculatorState();
}

class _CalculatorState extends State<Calculator> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              '0',
            ),
            Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    RaisedButton(
                      onPressed: () {},
                      child: Text('AC'),
                    ),
                    RaisedButton(
                      onPressed: () {},
                      child: Text('+-'),
                    ),
                    RaisedButton(
                      onPressed: () {},
                      child: Text('%'),
                    ),
                    RaisedButton(
                      onPressed: () {},
                      child: Text('/'),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    RaisedButton(
                      onPressed: () {},
                      child: Text('7'),
                    ),
                    RaisedButton(
                      onPressed: () {},
                      child: Text('8'),
                    ),
                    RaisedButton(
                      onPressed: () {},
                      child: Text('9'),
                    ),
                    RaisedButton(
                      onPressed: () {},
                      child: Text('x'),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    RaisedButton(
                      onPressed: () {},
                      child: Text('4'),
                    ),
                    RaisedButton(
                      onPressed: () {},
                      child: Text('5'),
                    ),
                    RaisedButton(
                      onPressed: () {},
                      child: Text('6'),
                    ),
                    RaisedButton(
                      onPressed: () {},
                      child: Text('-'),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    RaisedButton(
                      onPressed: () {},
                      child: Text('1'),
                    ),
                    RaisedButton(
                      onPressed: () {},
                      child: Text('2'),
                    ),
                    RaisedButton(
                      onPressed: () {},
                      child: Text('3'),
                    ),
                    RaisedButton(
                      onPressed: () {},
                      child: Text('4'),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    RaisedButton(
                      onPressed: () {},
                      child: Text('0'),
                    ),
                    RaisedButton(
                      onPressed: () {},
                      child: Text('.'),
                    ),
                    RaisedButton(
                      onPressed: () {},
                      child: Text('='),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
