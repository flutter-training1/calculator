import 'package:flutter/material.dart';

class Calculator extends StatefulWidget {
  Calculator({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _CalculatorState createState() => _CalculatorState();
}

// class MyButton {
//   final String title;
//   final VoidCallback callback;

//   MyButton({this.title, this.callback});
// }

class _CalculatorState extends State<Calculator> {
  // var numbers2 = [
  //   [
  //     MyButton(title: 'AC', callback: () {}),
  //     MyButton(title: '+-', callback: () {}),
  //     MyButton(title: '%', callback: () {}),
  //     MyButton(title: '/', callback: () {}),
  //   ]
  // ];
  // List<Widget> createButtons2(List<Map<String, VoidCallback>> numbers) {
  //   return numbers.map((item) {
  //     return Row(
  //       mainAxisAlignment: MainAxisAlignment.spaceAround,
  //       children: <Widget>[
  //         RaisedButton(
  //           onPressed: () {},
  //           child: Text('AC'),
  //         ),
  //         RaisedButton(
  //           onPressed: () {},
  //           child: Text('+-'),
  //         ),
  //         RaisedButton(
  //           onPressed: () {},
  //           child: Text('%'),
  //         ),
  //         RaisedButton(
  //           onPressed: () {},
  //           child: Text('/'),
  //         ),
  //       ],
  //     );
  //   }).toList();
  // }

  String displayNumber = '0';
  String number = '';
  String pre_number = '';
  String op = '';
  bool waitingNewNumber = false;

  List<Widget> createButtons(Map<String, VoidCallback> buttons) {
    var result = List<Widget>();

    result = buttons.entries.map((btn) {
      return RaisedButton(
        onPressed: btn.value,
        child: Text(btn.key),
      );
    }).toList();

    return result;
  }

  List<Widget> createRows(List<Map<String, VoidCallback>> numbers) {
    var result = List<Widget>();

    result = numbers.map((item) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: createButtons(item),
      );
    }).toList();

    return result;
  }

  String appendNumber(String newNumber) {
    if (waitingNewNumber) {
      waitingNewNumber = false;
      return newNumber;
    }

    if (number == '0') {
      return newNumber;
    } else {
      return number + newNumber;
    }
  }

  String calculate() {
    switch (op) {
      case '+':
        return (int.parse(pre_number) + int.parse(number)).toString();
      case '-':
        return '';
      case 'x':
        return '';
      case '/':
        return '';
      default:
        return '';
    }
  }

  @override
  Widget build(BuildContext context) {
    final numbers = [
      <String, VoidCallback>{
        'AC': () {
          print('AC');
        },
        '+-': () {
          print('+-');
        },
        '%': () {
          print('%');
        },
        '/': () {
          print('/');
        },
      },
      <String, VoidCallback>{
        '7': () {
          var result = appendNumber('7');
          setState(() {
            displayNumber = result;
            number = result;
          });
        },
        '8': () {
          var result = appendNumber('8');
          setState(() {
            displayNumber = result;
            number = result;
          });
        },
        '9': () {
          var result = appendNumber('9');
          setState(() {
            displayNumber = result;
            number = result;
          });
        },
        'x': () {},
      },
      <String, VoidCallback>{
        '4': () {
          var result = appendNumber('4');
          setState(() {
            displayNumber = result;
            number = result;
          });
        },
        '5': () {
          var result = appendNumber('5');
          setState(() {
            displayNumber = result;
            number = result;
          });
        },
        '6': () {
          var result = appendNumber('6');
          setState(() {
            displayNumber = result;
            number = result;
          });
        },
        '-': () {},
      },
      <String, VoidCallback>{
        '1': () {
          var result = appendNumber('1');
          setState(() {
            displayNumber = result;
            number = result;
          });
        },
        '2': () {
          var result = appendNumber('2');
          setState(() {
            displayNumber = result;
            number = result;
          });
        },
        '3': () {
          var result = appendNumber('3');
          setState(() {
            displayNumber = result;
            number = result;
          });
        },
        '+': () {
          setState(() {
            op = '+';
            pre_number = number;
            waitingNewNumber = true;
          });
        },
      },
      <String, VoidCallback>{
        '0': () {
          setState(() {
            number = appendNumber('0');
          });
        },
        '.': () {},
        '=': () {
          var result = calculate();
          setState(() {
            displayNumber = result;
            pre_number = result;
          });
        },
      },
    ];
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              displayNumber,
              style: TextStyle(
                fontSize: 34,
              ),
            ),
            Column(
              children: createRows(numbers),
            ),
          ],
        ),
      ),
    );
  }
}
